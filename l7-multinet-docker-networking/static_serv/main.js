const targetApi = '/api/make_ticket';
function nextTicket() {
    let displayArea = $('#textarea');
    $.post(targetApi, (data) => {
        displayArea.append(JSON.stringify(data) + '<br/>');
    }).fail(() => { alert('API call failed!'); });
}
