import os
import json
import redis
from flask import Flask, jsonify, request


app = Flask(__name__)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS', 'localhost')
    NAME = 'ticket_counter'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

@app.route('/make_ticket', methods=['POST'])
def post_factor_job():
    ret = RedisResource.conn.incr(
        RedisResource.NAME
    )
    return jsonify({'ticket': ret})
