#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

docker run \
  --name mariadb-node \
  -d \
  -v $DIR/conf.d:/etc/mysql/conf.d \
  -v $DIR/data:/var/lib/mysql \
  -p 3306:3306 \
  -p 4567:4567/udp \
  -p 4567-4568:4567-4568 \
  -p 4444:4444 \
  --restart=always \
  mariadb:10.1 \
  --wsrep_node_address=$(ip -4 addr ls en0 | awk '/inet / {print $2}' | cut -d"/" -f1)
