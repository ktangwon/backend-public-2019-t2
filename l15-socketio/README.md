## Today's Agenda

Let's build a "shoutbox." This is a display board where everyone can leave a message for display in real time. 


### For a quick demo:
* <BASE_URL>/app/index.html
* <BASE_URL>/app/sender.html

### Documentation
* On the Python side: https://python-socketio.readthedocs.io/en/latest/
* On the JS side: https://socket.io/docs/

