import logging
import socketio
import eventlet
import eventlet.wsgi
import json
from html_sanitizer import Sanitizer
from flask import Flask, send_from_directory, jsonify

LOG = logging.getLogger(name='main-srv')
FORMAT = '[%(levelname)s] %(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)
LOG.setLevel(logging.DEBUG)
socket_io = socketio.Server()
app = Flask(__name__)

sanitizer = Sanitizer()  # default configuration

@app.route('/')
def index():
    """Serve the client-side application."""
    return jsonify({'status': 'OK'})

@app.route('/app/<path:path>')
def get_cli(path):
    return send_from_directory('cli', path)


@socket_io.on('connect', namespace='/chat')
def connect(sid, environ):
    LOG.info('connected from %s', sid)

@socket_io.on('chat message', namespace='/chat')
def message(sid, data):
    raw_text = data.get('msg', '')
    raw_text = sanitizer.sanitize(raw_text)
    from datetime import datetime
    LOG.info("message %r from %s", data, sid)
    data['timestamp'] = datetime.now().isoformat()
    data['msg'] = raw_text
    socket_io.emit('reply', data=data, room=None)
    # room = None means broadcast; otherwise, send to that specific room

@socket_io.on('disconnect', namespace='/chat')
def disconnect(sid):
    LOG.info('disconnect %s', sid)

if __name__ == '__main__':
    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(socket_io, app)

    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 8000)), app)
