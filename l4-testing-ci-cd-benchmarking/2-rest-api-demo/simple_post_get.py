#!/usr/bin/env python3
"""
    This short program demonstrates how to make HTTP calls to API using
    a package called requests.
"""

import requests

BASE_URL = 'http://127.0.0.1:5000'
STATUS_OK = requests.codes['ok']

def sample_get_request():
    """ an example of how to make a GET request """
    resp = requests.get(BASE_URL+'/status')
    if resp.status_code == STATUS_OK:
        print('Response: STATUS OK', resp.status_code)
    else:
        print('Response: NOT OK', resp.status_code)
    print('json: {}'.format(resp.json()))
    print('text: {}'.format(resp.text))

def sample_post_request():
    """ an example of how to make a POST request """
    resp = requests.post(
        BASE_URL+'/vote',
        json={'vote_for': '5881072', 'your_id': '5987302'})
    if resp.status_code == STATUS_OK:
        print('Response: STATUS OK', resp.status_code)
    else:
        print('Response: NOT OK', resp.status_code)
    print('json: {}'.format(resp.json()))
    print('text: {}'.format(resp.text))


if __name__ == '__main__':
    sample_get_request()
    sample_post_request()
